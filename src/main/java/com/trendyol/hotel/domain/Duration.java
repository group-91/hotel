package com.trendyol.hotel.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.trendyol.common.constants.DatePatternConstants;
import lombok.*;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
public class Duration {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DatePatternConstants.DATE_PATTERN)
    private Date startDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DatePatternConstants.DATE_PATTERN)
    private Date endDate;

    public Duration() {
        this.startDate = new Date();
        this.endDate = new Date();
    }
}
