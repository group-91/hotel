package com.trendyol.hotel.domain;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
public class PriceRange {
    double lowPriceLimit;
    double highPriceLimit;

    public PriceRange() {
        this.lowPriceLimit = 0;
        this.highPriceLimit = 0;
    }
}
