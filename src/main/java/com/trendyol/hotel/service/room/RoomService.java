package com.trendyol.hotel.service.room;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.trendyol.common.model.BookingDTO;
import com.trendyol.hotel.contract.request.SearchRequest;
import com.trendyol.hotel.domain.Hotel;
import com.trendyol.hotel.domain.Room;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface RoomService {

	void updateRoom(BookingDTO bookingDTO);

	void addBookedDate(BookingDTO bookingDTO);

	List<Room> getAvailableRoomsOfHotel(String id, SearchRequest searchRequest);

    void addRoomToHotel(Room room);

	void deleteRoom(String hotelId, String roomId);
}
