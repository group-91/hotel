package com.trendyol.hotel.repository.hotel;

import com.trendyol.hotel.contract.request.SearchRequest;
import com.trendyol.hotel.domain.Hotel;
import com.trendyol.hotel.domain.Room;

import java.util.List;
import java.util.Optional;

public interface HotelRepository {

	void insert(Hotel hotel);

	void update(Hotel hotel);

	Optional<Hotel> findById(String id);

	//Optional<List<Hotel>> findAll();

    Optional<List<Hotel>> getSuitableHotels(SearchRequest searchRequest);

    void delete(String id);
}
