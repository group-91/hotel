package com.trendyol.hotel.repository.room;

import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.query.QueryResult;
import com.trendyol.common.model.BookingDTO;
import com.trendyol.hotel.contract.request.SearchRequest;
import com.trendyol.hotel.domain.Hotel;
import com.trendyol.hotel.domain.Room;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.text.Format;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Repository
@AllArgsConstructor
public class RoomRepositoryImpl implements RoomRepository {

	private final Cluster couchbaseCluster;
	private final Format dateFormatter;

	@Override
	public boolean checkRoomAvailability(BookingDTO bookingDTO) {
		return checkRoomAvailability(bookingDTO.getHotelId(),bookingDTO.getRoomId(),bookingDTO.getStartDate(),bookingDTO.getEndDate());
	}

	@Override
	public void addBookedDate(BookingDTO bookingDTO) {
			String requestBeginDate = dateFormatter.format(bookingDTO.getStartDate());
			String requestEndDate = dateFormatter.format(bookingDTO.getEndDate());
			String statement = String.format("UPDATE hotel SET room.bookedDates = ARRAY_APPEND (room.bookedDates, {\"startDate\":\"%s\" , \"endDate\":\"%s\"}) FOR room in rooms when room.id == \"%s\" END WHERE id = \"%s\"", requestBeginDate,requestEndDate,bookingDTO.getRoomId(),bookingDTO.getHotelId());
			couchbaseCluster.query(statement);
	}

	public boolean checkRoomAvailability(String hotelId, String roomId,Date startDate, Date endDate) {
		String requestBeginDate = dateFormatter.format(startDate);
		String requestEndDate = dateFormatter.format(endDate);

		String statement = String.format("Select rooms from hotel WHERE " +
				"id = \"%s\" "+
				"and ANY r IN rooms SATISFIES "+
				"r.id == \"%s\" and ANY bookedDate IN r.bookedDates SATISFIES "+
				"( \"%s\"<bookedDate.endDate and "+
				"\"%s\">bookedDate.beginDate ) "+
				"END END;", hotelId, roomId, requestBeginDate, requestEndDate);
		QueryResult query = couchbaseCluster.query(statement);
		List<Room> result = query.rowsAs(Room.class);

		if(result.size()>0) return false;
		return true;
	}

	@Override
	public Optional<List<Room>> getAvailableRoomsOfHotel(Hotel hotel, SearchRequest searchRequest) {
		String requestBeginDate = dateFormatter.format(searchRequest.getBeginDate());
		String requestEndDate = dateFormatter.format(searchRequest.getEndDate());
		String statement = String.format(Locale.US, "Select rooms from hotel WHERE " +
						"hotelId == \"%s\" " +
						"and ANY r IN rooms SATISFIES " +
						"( r.nightlyPrice between %.2f and %.2f ) and " +
						"( ARRAY_LENGTH(room.bookDates)=0 or " +
						"EVERY bookedDate IN r.bookedDates SATISFIES " +
						"( (\"%s\">=bookedDate.endDate ) or "+
						"(\"%s\"<= bookedDate.beginDate ) ) "+
						"END END;", hotel.getId(),
				searchRequest.getLowPriceLimit(), searchRequest.getHighPriceLimit(),
				requestBeginDate, requestEndDate
		);
		QueryResult query = couchbaseCluster.query(statement);
		List<Room> result = query.rowsAs(Room.class);
		if(result.size()==0)
			return Optional.empty();
		return Optional.of(result);
	}

}
