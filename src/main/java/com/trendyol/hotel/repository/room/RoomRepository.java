package com.trendyol.hotel.repository.room;

import com.trendyol.common.model.BookingDTO;
import com.trendyol.hotel.contract.request.SearchRequest;
import com.trendyol.hotel.domain.Hotel;
import com.trendyol.hotel.domain.Room;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface RoomRepository {

	boolean checkRoomAvailability(BookingDTO bookingDTO);

	void addBookedDate(BookingDTO bookingDTO);

	boolean checkRoomAvailability(String hotelId, String roomId, Date startDate, Date endDate);

	Optional<List<Room>> getAvailableRoomsOfHotel(Hotel hotel, SearchRequest searchRequest);

}
