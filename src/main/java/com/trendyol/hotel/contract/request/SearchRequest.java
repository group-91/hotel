package com.trendyol.hotel.contract.request;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

@Getter
@Setter
@Builder
public class SearchRequest {
    private int pageNo;
    private int pageSize;
    private Date beginDate;
    @NonNull
    private Date endDate;
    private double lowPriceLimit;
    private double highPriceLimit;
}
